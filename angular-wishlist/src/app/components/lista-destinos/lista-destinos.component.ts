import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje} from '../../models/destino-viaje-model';
import { TareasApiClient } from '../../models/tareas-api-client';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AppState } from '../../app.module';
import { select, Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from '../../models/tarea-state-model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [TareasApiClient]
})
export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>
  updates: string[];
  all;
  constructor(public tareasApiClient: TareasApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.tareas.favorito).subscribe(d =>{
      if (d != null){
        this.updates.push('se ha elegido a ' + d.tarea);
      }
    });
    this.all = store.select(state => state.tareas.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }

  agregado(t: DestinoViaje){
    this.tareasApiClient.add(t);
    this.onItemAdded.emit(t);
    console.log(this.all);
  }
  elegido(d: DestinoViaje){
    this.tareasApiClient.elegir(d);
  }
  getAll(){

  }
}
