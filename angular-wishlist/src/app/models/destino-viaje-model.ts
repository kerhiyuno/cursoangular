import * as uuid from 'uuid';
export class DestinoViaje{
    voteDown() {
    if (this.votes>0){
      this.votes--;}
    }
    voteUp() {
      this.votes++;
    }
    voteReinicio() {
        this.votes=0;
      }
    private selected: boolean;
    public metas: string[];
    id= uuid.v4();
    constructor(public tarea: string,public imagenUrl: string,public descripcion: string, public votes: number = 0){
        this.metas=['terminar en 1 hora','no cometer errores'];
    }
    isSelected(): boolean{
        return this.selected;
    }
    setSelected(s: boolean){
        this.selected=s;
    }
}
