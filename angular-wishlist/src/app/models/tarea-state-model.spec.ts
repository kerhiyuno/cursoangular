import {
    reducerDestinosViajes,
    DestinosViajesState,
    initializeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction
  } from './tarea-state-model';
  import { DestinoViaje } from './destino-viaje-model';
  
  describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
      const prevState: DestinosViajesState = initializeDestinosViajesState();
      const action: InitMyDataAction = new InitMyDataAction(['tarea 1', 'tarea 2']);
      const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      expect(newState.items.length).toEqual(2);
      expect(newState.items[0].tarea).toEqual('tarea 1');
    });
  
    it('should reduce new item added', () => {
      const prevState: DestinosViajesState = initializeDestinosViajesState();
      const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url','descripcion'));
      const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      expect(newState.items.length).toEqual(1);
      expect(newState.items[0].tarea).toEqual('barcelona');
    });
  });
  